parse-linear-inequ-system
=============

# License
Licensed under Boost 1.0 (See accompanying file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)

# Summary
Some Haskell code for representing linear inequality systems with rational coefficients and parsing text representations of these systems. This repository was part of larger hobby effort for playing with math from scratch in Haskell from 2014. This repository is mostly a demo of using the parsing library [parsec](https://github.com/haskell/parsec) and the testing library [tasty](https://github.com/UnkindPartition/tasty).

#  Example:
Code like this
```
import ParseLinearSystem (parseSystem)

parseSystem "
3/1 x_0  - 12316/324 x_1          + 1 x_2    = 234/88888888234123412;
-7x_0         + -3/1 x_1                     <= -100;
1 x_0                      - 82340/23 x_2    >= 2;"
```
will parse the above to a repsentation of
```math
% gitlab had problems renders align so I stuck with matrix
\begin{matrix}
3 x_0 & - \frac{12316}{324} x_1     &     + 1 x_2 \\
-7x_0  &        -\frac{3}{1} x_1  \\
1 x_0   & &                   -\frac{82340}{23} x_2 
\end{matrix} 
\begin{matrix}
= \\
\le \\
\ge
\end{matrix} 
\begin{matrix}
\frac{234}{88888888234123412} \\
-100 \\
2
\end{matrix}.
```


