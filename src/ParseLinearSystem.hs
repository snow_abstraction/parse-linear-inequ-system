--          Copyright Douglas W. Potter 2014.
-- Distributed under the Boost Software License, Version 1.0.
--    (See accompanying file LICENSE_1_0.txt or copy at
--          http://www.boost.org/LICENSE_1_0.txt)
module ParseLinearSystem
  (parseSystem, parseLinearInequality,
   parseTerms, parseDegreeOneMonomial, parseSense, parseRational)
       where
import Text.ParserCombinators.Parsec
import qualified Text.Parsec.Prim as Prim hiding (try)
import Text.ParserCombinators.Parsec.Language (emptyDef)
import qualified Text.ParserCombinators.Parsec.Token as PT
import Control.Applicative ((<$>), (*>), (<*>))
import Data.List ()
import Data.Ratio ((%))
import Data.Word (Word)
import qualified LinearSystem as LS

--TODO rewrite more in applicative style
--TODO restrict what is exported, ? how to do this and support testing some internals?

-- primary parse* functions
-- each "parseX" function is implemented by a corresponding
-- "p_X" where X is what is intended to be parsed.
parseSystem :: String -> Either ParseError LS.System
parseSystem input = parse p_System "" input

parseLinearInequality :: String -> Either ParseError LS.LinearInequality
parseLinearInequality input = parse p_LinearInequality "" input

parseTerms :: String -> Either ParseError LS.Terms
parseTerms input = parse p_Terms "" input

parseDegreeOneMonomial :: String -> Either ParseError LS.DegreeOneMonomial
parseDegreeOneMonomial input = parse p_DegreeOneMonomial "" input

parseSense :: String -> Either ParseError LS.Sense
parseSense input = parse p_Sense "" input

parseRational :: String -> Either ParseError Rational
parseRational input = parse p_Rational "" input


lexer = PT.makeTokenParser emptyDef
integer = PT.integer lexer
natural = PT.natural lexer

-- TODO: instead of ';' use '\n' as the separator.
-- Note even if usages below of spaces changed to something that only
-- skips ' ', some other parser is skipping '\n'.
p_System :: GenParser Char st LS.System
p_System = endBy1 p_LinearInequality eol
  where
    eol = char ';'

p_LinearInequality :: GenParser Char st LS.LinearInequality
p_LinearInequality =
  LS.LinearInequality <$> p_Terms <*> p_Sense <*> p_Rational

p_Sense :: GenParser Char st LS.Sense
p_Sense = spaces *> (badValues *> goodValues <?> "<=, >= or =")
 where 
   badValues =  foldr1 (>>) (map (notFollowedBy . string)  ["==", "=>", "=<"])
   goodValues = foldr1 (<|>)
     [(string "<=") *> (Prim.parserReturn LS.LesserOrEqual) 
     ,(string ">=") *> (Prim.parserReturn LS.GreaterOrEqual)
     ,(string  "=") *> (Prim.parserReturn LS.Equal)]

p_Terms ::  GenParser Char st LS.Terms
p_Terms = (try p) <?>
  "an expression of DegreeOneMonomials with unique variable indices"
  where
    p = do
      spaces
      x <- p_DegreeOneMonomial
      xs <- pRemaining
-- While this way of checking for repeating varaible indicies gives quadratic
-- complexity in the number of variables (when reading), it does show 
-- the location of the duplication in the input without much coding work.
-- The complexity could be reduced by scaning the entire list but then 
-- locating the error in the input will be harded or maintaining some 
-- sorted data structure.
      let i = LS.variableIndex x
      if i `elem` usedIndices xs
        then fail $ "variable index " ++ (show i) ++ " repeated"
        else return $ x : xs
    pRemaining = (try (lookAhead $ char '-') *> p)
      <|> (char '+' *> p)
      <|> (return [])
    usedIndices = map LS.variableIndex

p_DegreeOneMonomial :: GenParser Char st LS.DegreeOneMonomial
p_DegreeOneMonomial = do
  r <- p_Rational
  string LS.coefficientIndexSep
  spaces
  index <- p_Word
  return $ LS.DegreeOneMonomial {
    LS.coefficient = r,
    LS.variableIndex = index}

p_Rational :: GenParser Char st Rational
p_Rational = spaces *> ((%) <$> integer <*> parseDenominator)
  where
    parseDenominator = (notFollowedBy (string LS.fracSep) *> return 1)
      <|> ((string LS.fracSep) >> spaces >> p_NonZeroInteger)

p_NonZeroInteger :: GenParser Char st Integer
p_NonZeroInteger = try p <?> "a nonzero integer"
 where p = do
         x <- integer
         case x of
           0 -> fail ""
           _ -> return x

p_Word :: GenParser Char st Word
p_Word = do
  n <- natural
  let w = fromInteger n :: Word
  if toInteger w == n then
    return w
    else fail $ "index parsed as " ++ (show n) ++
         "unrepresentable as index type Word (unsigned Int)."
